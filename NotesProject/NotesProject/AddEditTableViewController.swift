//
//  AddEditTableViewController.swift
//  NotesProject
//
//  Created by user192474 on 3/4/21.
//

import CoreLocation
import MapKit
import UIKit

class AddEditTableViewController: UITableViewController, UITextFieldDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var TitreTF: UITextField!
    
    @IBOutlet weak var ContenuTF: UITextField!
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
        
    @IBOutlet weak var map: MKMapView!
    
    @IBOutlet weak var imageLib: UIImageView!
    
    let manager = CLLocationManager()
    
    var note:Notes?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        TitreTF.delegate = self
        ContenuTF.delegate = self
        
        if let note:Notes = self.note{
            saveButton.isEnabled=true;
            TitreTF.text = note.titre
            ContenuTF.text = note.contenu
            if let imageData:Data = note.photo as Data?{
                imageLib.image = UIImage(data: imageData)
            }
        }
        else{
            saveButton.isEnabled=false;
        }
        
    }
    
    @IBAction func didTapButton(){
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.delegate = self
        vc.allowsEditing = true
        present(vc, animated: true)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first{
            manager.stopUpdatingLocation()
            render(location)
        }
    }
    
    func render(_ location:CLLocation){
        let coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        
        let region = MKCoordinateRegion(center: coordinate, span: span)
        
        map.setRegion(region, animated: true)
        
        let pin = MKPointAnnotation()
        pin.coordinate = coordinate
        map.addAnnotation(pin)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let Titretext :String
        let Contenutext :String
        if textField == TitreTF{
            Titretext = (TitreTF.text! as NSString).replacingCharacters(in: range, with: string)
            Contenutext = ContenuTF.text!
        }
        else if textField == ContenuTF{
            Titretext = TitreTF.text!
            Contenutext = (ContenuTF.text! as NSString).replacingCharacters(in: range, with: string)
        }else {
            Titretext = ""
            Contenutext = ""
        }
        
        if !Titretext.isEmpty && !Contenutext.isEmpty{
            saveButton.isEnabled=true;
        } else {
            saveButton.isEnabled=false;
        }

        return true
    }
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    

    // MARK: - Table view data source

    /*
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }
    */

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "saveUnwind" {
            let titre = TitreTF.text ?? ""
            let contenu = ContenuTF.text ?? ""
            let txt = "Derniere modif le : "
            let now = Date()
            let french       = DateFormatter()
            french.dateStyle = .medium
            french.timeStyle = .medium
            french.locale    = Locale(identifier: "FR-fr")
            let photo = imageLib.image?.pngData()
            self.note = Notes(titre: titre, contenu: contenu, date: txt + french.string(from: now),photo: photo as NSData?)
        }
    }
}

extension AddEditTableViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")] as? UIImage{
            imageLib.image = image
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

    

	
