//
//  NotesTableViewController.swift
//  NotesProject
//
//  Created by user192474 on 3/4/21.
//

import UIKit

class NotesTableViewController: UITableViewController {

    var notes: [Notes] = [
        Notes(titre: "Note1", contenu: "Ceci est la premiere note",
              date: "Derniere modif le : 7 mars 2021 à 21:12:25", photo: nil),
        Notes(titre: "Note 2", contenu: "Ceci est la deuxieme note",
              date: "Derniere modif le : 4 mars 2021 à 09:03:53", photo: nil),
        Notes(titre: "Note 3", contenu: "Ceci est la troisieme note",
              date: "Derniere modif le : 11 mars 2021 à 14:32:12", photo: nil)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.leftBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return notes.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoteCell", for: indexPath)

        // Configure the cell...

        let note = notes[indexPath.row]
        cell.textLabel?.text = "\(note.titre)"
        cell.detailTextLabel?.text = note.date
        cell.showsReorderControl = true
        
        return cell
    }

    
    @IBAction func unwindToNoteTableView(segue: UIStoryboardSegue){
        if segue.identifier == "saveUnwind" {
            let sourceVC = segue.source as! AddEditTableViewController
            
            if let note = sourceVC.note{
                if let selectedIndexPath = tableView.indexPathForSelectedRow{
                    //modif
                    self.notes[selectedIndexPath.row] = note
                    tableView.reloadData()
                }
                else{
                    //ajout
                    self.notes.append(note)
                    tableView.reloadData()
                }
            }
        }
        
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            notes.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    

    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        
        let movedNote = notes.remove(at: fromIndexPath.row)
        notes.insert(movedNote, at: to.row)
        tableView.reloadData()

    }

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
     if segue.identifier == "EditNote" {
         
         
         let indexPath = tableView.indexPathForSelectedRow!
         let note = notes[indexPath.row]
         let navController = segue.destination as! UINavigationController
         let addEditVC = navController.topViewController as! AddEditTableViewController
         addEditVC.note = note
     }
 }

}
