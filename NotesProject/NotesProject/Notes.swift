//
//  Notes.swift
//  NotesProject
//
//  Created by user192474 on 3/4/21.
//

import Foundation

class Notes {
    var titre: String
    var contenu: String
    var date: String
    var photo: NSData?
    
    init(titre: String, contenu: String, date: String, photo:NSData?) {
        self.titre = titre
        self.contenu = contenu
        self.date = date
        self.photo = photo
    }
}
